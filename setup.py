# This Python file uses the following encoding: utf-8
from setuptools import setup
import os


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(name='django-rml-pdf-generator',
      version='0.0.1',
      description='',
      author='Rample.com',
      author_email='info@rample.com',
      url='https://bitbucket.org/rample/pdf_creator/',
      include_package_data=True,
      packages=['pdf_generator'],
      install_requires=[
          'setuptools',
          'z3c.rml',
          'django',
      ],
      zip_safe=False,
      long_description=read('README.rst'),
      package_data={
        'pdf_generator': ['*.rml'],
      },
      classifiers=['Development Status :: 4 - Beta',
                   'Environment :: Web Environment',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: BSD License',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Topic :: Utilities'],
      )
