from z3c.rml import rml2pdf
from django.http import HttpResponse
from django.template import loader, Context
import reportlab.rl_config
import os
from functools import wraps


def pdf_render_to_response(template_name, **trigger):

    def decorator(func):
        @wraps(func)
        def inner(request, *args, **kw):
            response = HttpResponse(mimetype='application/pdf')
            filename = trigger.get('filename', None) or os.path.basename(template_name)
            prompt = trigger.get('prompt', False)

            reportlab.rl_config.warnOnMissingFontGlyphs = 0
            cd = []

            if prompt:
                cd.append('attachment')

            cd.append('filename=%s' % filename)
            response['Content-Disposition'] = '; '.join(cd)

            tpl = loader.get_template(template_name)

            tc = {'filename': filename}
            context = func(request, *args, **kw)
            tc.update(context)

            ctx = Context(tc)
            rendered = tpl.render(ctx)
            pdf = rml2pdf.parseString(rendered)
            response.write(pdf.read())
            return response
        return inner
    return decorator
