************************
django-rml-pdf-generator
************************

============
Installation
============
Install dependencies, unless you install pdf-generator as egg

    $ easy_install z3c.rml

Add pdf_generator to your INSTALLED_APPS:

   INSTALLED_APPS = 
    (...)
    'pdf_generator',
    (...)

===========
Usage
===========
Put something like that in your view

    from pdf_generator.decorators import pdf_render_to_response

    @pdf_render_to_response('test.rml', filename='mytest', prompt=True)
    def my_view(request, *args, **kw):
        return {'k':'v'}

Decorator arguments:
  * template_name (required)
  * filename - self explanatory
  * prompt - if browser should ask to save pdf

Template should look like that:

    {% extends "base.rst" %}
    {% block content %}
    <para style="default">Lorem ipsum dolor sit amet</para>
    {% endblock %}

    {% block style %}
        <paraStyle 
            name="default"
            fontName="DejaVuSans"
            fontSize="7"
            leading="9"
            alignment="LEFT"
            textColor="black"
            />
    {% endblock %}

Provided base template defines 3 blocks:
 * template - for page templates (defaults to A4)
 * style - for custom styles 
 * content - for content

For futher info see `RML documentation <https://github.com/zopefoundation/z3c.rml/blob/master/src/z3c/rml/rml-reference.pdf?raw=true>`_.

font DejaVuSans is included in this distribution (maps to font names: DejaVuSans, DejaVuSans-Bold, DejaVuSans-Oblique, DejaVuSans-BoldOblique)
